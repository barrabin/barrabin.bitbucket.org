'use strict';

/* html2dom */


/* Map value from range start and end to new range toStart toEnd 
 * bounded to min toStart, max at toEnd
 */


/* EaseInOutQuad: 
 *  acceleration until halfway, then deceleration
 * t => [0,1]
 */


/* Passive event detector */
var supportsPassiveEv = false;
try {
  var opts = Object.defineProperty({}, 'passive', {
    get: function get() {
      supportsPassiveEv = true;
    }
  });
  window.addEventListener("test", null, opts);
} catch (e) {}

/*
 * Scroll throttle
 */
function __throttle(type, name, obj) {
  var obj = obj || window;
  var running = false;
  var func = function func() {
    if (running) {
      return;
    }
    running = true;
    requestAnimationFrame(function () {
      obj.dispatchEvent(new CustomEvent(name));
      running = false;
    });
  };
  obj.addEventListener(type, func, supportsPassiveEv ? { passive: true } : false);
}

/* Cast some iterable to array */
function cast2array(it) {
  return Array.prototype.slice.call(it);
}

/* 
 * Bound Recurse call f() at most N times. 
 *  Returns f()[1]
 *
 * if f()[0] == false , continue recursive call
 * else stop and return f()[1]
 */


/*
 * Get parent who has class className.
 * Traverse 3 generations.
 */


/*
 * Css helpers
 */
 //(document.body.style.setProperty !== undefined)



/*
 * Create CSS Sheet
 */


/**
 * Parse query string
 */
function parseQuery(qstr) {
  var query = {};
  var a = (qstr[0] === '?' ? qstr.substr(1) : qstr).split('&');
  for (var i = 0; i < a.length; i++) {
    var b = a[i].split('=');
    query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
  }
  return query;
}

const Lazyframe = () => {

  let settings;

  const elements = [];

  const defaults = {
    vendor: undefined,
    id: undefined,
    src: undefined,
    thumbnail: undefined,
    title: undefined,
    apikey: undefined,
    initialized: false,
    parameters: undefined,
    y: undefined,
    debounce: 250,
    lazyload: true,
    initinview: false,
    onLoad: (l) => {},
    onAppend: (l) => {},
    onThumbnailLoad: (img) => {}
  };

  const constants = {
    regex: {
      youtube: /(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/,
      vimeo: /vimeo\.com\/(?:video\/)?([0-9]*)(?:\?|)/,
      vine: /vine.co\/v\/(.*)/
    },
    condition: {
      youtube: (m) => (m && m[1].length == 11) ? m[1] : false,
      vimeo: (m) => (m && m[1].length === 9 || m[1].length === 8) ? m[1] : false,
      vine: (m) => (m && m[1].length === 11) ? m[1] : false
    },
    src: {
      youtube: (s) => `https://www.youtube.com/embed/${s.id}/?${s.parameters}`,
      vimeo: (s) => `https://player.vimeo.com/video/${s.id}/?${s.parameters}`,
      vine: (s) => `https://vine.co/v/${s.id}/embed/simple`
    },
    endpoints: {
      youtube: (s) => `https://www.googleapis.com/youtube/v3/videos?id=${s.id}&key=${s.apikey}&fields=items(snippet(title,thumbnails))&part=snippet`,
      vimeo: (s) => `https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/${s.id}`,
      vine: (s) => `https://vine.co/oembed.json?url=https%3A%2F%2Fvine.co%2Fv%2F${s.id}`
    },
    response: {
      youtube: {
        title: (r) => r.items['0'].snippet.title,
        thumbnail: (r) => {
          let thumb = r.items["0"].snippet.thumbnails;
          let url = thumb.maxres ? thumb.maxres.url : thumb.standard.url;
          return url;
        }
      },
      vimeo: {
        title: (r) => r.title,
        thumbnail: (r) => r.thumbnail_url
      },
      vine: {
        title: (r) => r.title,
        thumbnail: (r) => r.thumbnail_url
      }
    }
  };

  function init(elements, ...args) {
    settings = Object.assign({}, defaults, args[0]);

    if (typeof elements === 'string') {

      const selector = document.querySelectorAll(elements);
      for (let i = 0; i < selector.length; i++) {
        loop(selector[i]);
      }

    } else if (typeof elements.length === 'undefined'){
      loop(elements);

    } else if (elements.length > 1) {

      for (let i = 0; i < elements.length; i++) {
        loop(elements[i]);
      }

    } else {
      loop(elements[0]);
    }

    if (settings.lazyload) {
      scroll();
    }

  }

  function loop(el) {

    if(el instanceof HTMLElement === false ||
       el.classList.contains('lazyframe--loaded')) return;

    const lazyframe = {
      el: el,
      settings: setup(el),
    };

    lazyframe.el.addEventListener('click', () => {
      lazyframe.el.appendChild(lazyframe.iframe);

      const iframe = el.querySelectorAll('iframe');
      lazyframe.settings.onAppend.call(this, iframe[0]);
    });

    if (settings.lazyload) {
      build(lazyframe);
    } else {
      api(lazyframe, !!lazyframe.settings.thumbnail);
    }

  }

  function setup(el) {

    const attr = Array.prototype.slice.apply(el.attributes)
     .filter(att => att.value !== '')
     .reduce((obj, curr) => {
        let name = curr.name.indexOf('data-') === 0 ? curr.name.split('data-')[1] : curr.name;
        obj[name] = curr.value;
        return obj;
     }, {});

    const options = Object.assign({},
      settings,
      attr,
      {
        y: el.offsetTop,
        parameters: extractParams(attr.src)
      }
    );

    if (options.vendor) {
      const match = options.src.match(constants.regex[options.vendor]);
      options.id = constants.condition[options.vendor](match);
    }

    return options;

  }

  function extractParams(url) {
    let params = url.split('?');

    if (params[1]) {
      params = params[1];
      const hasAutoplay = params.indexOf('autoplay') !== -1;
      return hasAutoplay ? params : params + '&autoplay=1';

    } else {
      return 'autoplay=1';
    }

  }

  function useApi(settings) {

    if (!settings.vendor) return false;

    if (!settings.title || !settings.thumbnail) {
      if (settings.vendor === 'youtube') {
        return !!settings.apikey;
      } else {
        return true;
      }

    } else {
      return false;
    }

  }

  function api(lazyframe) {

    if (useApi(lazyframe.settings)) {
      send(lazyframe, (err, data) => {
        if (err) return;

        const response = data[0];
        const _l = data[1];

        if (!_l.settings.title) {
          _l.settings.title = constants.response[_l.settings.vendor].title(response);
        }
        if (!_l.settings.thumbnail) {
          const url = constants.response[_l.settings.vendor].thumbnail(response);
          _l.settings.thumbnail = url;
          lazyframe.settings.onThumbnailLoad.call(this, url);
        }
        build(_l, true);

      });

    }else{
      build(lazyframe, true);
    }

  }

  function send(lazyframe, cb) {

    const endpoint = constants.endpoints[lazyframe.settings.vendor](lazyframe.settings);
    const request = new XMLHttpRequest();

    request.open('GET', endpoint, true);

    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        const data = JSON.parse(request.responseText);
        cb(null, [data, lazyframe]);
      } else {
        cb(true);
      }
    };

    request.onerror = function() {
      cb(true);
    };

    request.send();

  }

  function scroll() {

    const height = window.innerHeight;
    let count = elements.length;
    const initElement = (el, i) => {
      el.settings.initialized = true;
      el.el.classList.add('lazyframe--loaded');
      count--;
      api(el);

      if (el.settings.initinview) {
        el.el.click();
      }

      el.settings.onLoad.call(this, el);
    };

    elements
      .filter(el => el.settings.y < height)
      .forEach(initElement);

    const onScroll = debounce(() => {

      up = lastY < window.scrollY;
      lastY = window.scrollY;

      if (up) {
        elements
          .filter(el => el.settings.y < (height + lastY) && el.settings.initialized === false)
          .forEach(initElement);
      }

      if (count === 0) {
        window.removeEventListener('scroll', onScroll, false);
      }

    }, settings.debounce);

    let lastY = 0;
    let t = false, up = false;
    window.addEventListener('scroll', onScroll, false);

    function debounce(func, wait, immediate) {
      let timeout;
      return function() {
        let context = this, args = arguments;
        let later = function() {
          timeout = null;
          if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
      };
    }

  }

  function build(lazyframe, loadImage) {

    lazyframe.iframe = getIframe(lazyframe.settings);

    if (lazyframe.settings.thumbnail && loadImage) {
      lazyframe.el.style.backgroundImage = `url(${lazyframe.settings.thumbnail})`;
    }

    if (lazyframe.settings.title && lazyframe.el.children.length === 0) {
      const docfrag = document.createDocumentFragment(),
            titleNode = document.createElement('span');

      titleNode.className = 'lazyframe__title';
      titleNode.innerHTML = lazyframe.settings.title;
      docfrag.appendChild(titleNode);

      lazyframe.el.appendChild(docfrag);
    }

    if (!settings.lazyload) {
      lazyframe.el.classList.add('lazyframe--loaded');
      lazyframe.settings.onLoad.call(this, lazyframe);
      elements.push(lazyframe);
    }

    if (!lazyframe.settings.initialized) {
      elements.push(lazyframe);
    }

  }

  function getIframe(settings) {

    const docfrag = document.createDocumentFragment(),
          iframeNode = document.createElement('iframe');

    if (settings.vendor) {
      settings.src = constants.src[settings.vendor](settings);
    }

    iframeNode.setAttribute('id', `lazyframe-${settings.id}`);
    iframeNode.setAttribute('src', settings.src);
    iframeNode.setAttribute('frameborder', 0);
    iframeNode.setAttribute('allowfullscreen', '');

    if (settings.vendor === 'vine') {
      const scriptNode = document.createElement('script');
      scriptNode.setAttribute('src', 'https://platform.vine.co/static/scripts/embed.js');
      docfrag.appendChild(scriptNode);
    }

    docfrag.appendChild(iframeNode);
    return docfrag;

  }

  return init;

};

const lf = Lazyframe();

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var fontfaceobserver_standalone = createCommonjsModule(function (module) {
/* Font Face Observer v2.0.13 - © Bram Stein. License: BSD-3-Clause */(function(){function l(a,b){document.addEventListener?a.addEventListener("scroll",b,!1):a.attachEvent("scroll",b);}function m(a){document.body?a():document.addEventListener?document.addEventListener("DOMContentLoaded",function c(){document.removeEventListener("DOMContentLoaded",c);a();}):document.attachEvent("onreadystatechange",function k(){if("interactive"==document.readyState||"complete"==document.readyState)document.detachEvent("onreadystatechange",k),a();});}function r(a){this.a=document.createElement("div");this.a.setAttribute("aria-hidden","true");this.a.appendChild(document.createTextNode(a));this.b=document.createElement("span");this.c=document.createElement("span");this.h=document.createElement("span");this.f=document.createElement("span");this.g=-1;this.b.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.c.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
this.f.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.h.style.cssText="display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";this.b.appendChild(this.h);this.c.appendChild(this.f);this.a.appendChild(this.b);this.a.appendChild(this.c);}
function t(a,b){a.a.style.cssText="max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;white-space:nowrap;font-synthesis:none;font:"+b+";";}function y(a){var b=a.a.offsetWidth,c=b+100;a.f.style.width=c+"px";a.c.scrollLeft=c;a.b.scrollLeft=a.b.scrollWidth+100;return a.g!==b?(a.g=b,!0):!1}function z(a,b){function c(){var a=k;y(a)&&a.a.parentNode&&b(a.g);}var k=a;l(a.b,c);l(a.c,c);y(a);}function A(a,b){var c=b||{};this.family=a;this.style=c.style||"normal";this.weight=c.weight||"normal";this.stretch=c.stretch||"normal";}var B=null,C=null,E=null,F=null;function G(){if(null===C)if(J()&&/Apple/.test(window.navigator.vendor)){var a=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))(?:\.([0-9]+))/.exec(window.navigator.userAgent);C=!!a&&603>parseInt(a[1],10);}else C=!1;return C}function J(){null===F&&(F=!!document.fonts);return F}
function K(){if(null===E){var a=document.createElement("div");try{a.style.font="condensed 100px sans-serif";}catch(b){}E=""!==a.style.font;}return E}function L(a,b){return[a.style,a.weight,K()?a.stretch:"","100px",b].join(" ")}
A.prototype.load=function(a,b){var c=this,k=a||"BESbswy",q=0,D=b||3E3,H=(new Date).getTime();return new Promise(function(a,b){if(J()&&!G()){var M=new Promise(function(a,b){function e(){(new Date).getTime()-H>=D?b():document.fonts.load(L(c,'"'+c.family+'"'),k).then(function(c){1<=c.length?a():setTimeout(e,25);},function(){b();});}e();}),N=new Promise(function(a,c){q=setTimeout(c,D);});Promise.race([N,M]).then(function(){clearTimeout(q);a(c);},function(){b(c);});}else m(function(){function u(){var b;if(b=-1!=
f&&-1!=g||-1!=f&&-1!=h||-1!=g&&-1!=h)(b=f!=g&&f!=h&&g!=h)||(null===B&&(b=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),B=!!b&&(536>parseInt(b[1],10)||536===parseInt(b[1],10)&&11>=parseInt(b[2],10))),b=B&&(f==v&&g==v&&h==v||f==w&&g==w&&h==w||f==x&&g==x&&h==x)),b=!b;b&&(d.parentNode&&d.parentNode.removeChild(d),clearTimeout(q),a(c));}function I(){if((new Date).getTime()-H>=D)d.parentNode&&d.parentNode.removeChild(d),b(c);else{var a=document.hidden;if(!0===a||void 0===a)f=e.a.offsetWidth,
g=n.a.offsetWidth,h=p.a.offsetWidth,u();q=setTimeout(I,50);}}var e=new r(k),n=new r(k),p=new r(k),f=-1,g=-1,h=-1,v=-1,w=-1,x=-1,d=document.createElement("div");d.dir="ltr";t(e,L(c,"sans-serif"));t(n,L(c,"serif"));t(p,L(c,"monospace"));d.appendChild(e.a);d.appendChild(n.a);d.appendChild(p.a);document.body.appendChild(d);v=e.a.offsetWidth;w=n.a.offsetWidth;x=p.a.offsetWidth;I();z(e,function(a){f=a;u();});t(e,L(c,'"'+c.family+'",sans-serif'));z(n,function(a){g=a;u();});t(n,L(c,'"'+c.family+'",serif'));
z(p,function(a){h=a;u();});t(p,L(c,'"'+c.family+'",monospace'));});})};module.exports=A;}());
});

var skrollr_min = createCommonjsModule(function (module) {
/*! skrollr 0.6.26 (2014-06-08) | Alexander Prinzhorn - https://github.com/Prinzhorn/skrollr | Free to use under terms of MIT license */
(function(e,t,r){"use strict";function n(r){if(o=t.documentElement,a=t.body,K(),it=this,r=r||{},ut=r.constants||{},r.easing)for(var n in r.easing)U[n]=r.easing[n];yt=r.edgeStrategy||"set",ct={beforerender:r.beforerender,render:r.render,keyframe:r.keyframe},ft=r.forceHeight!==!1,ft&&(Vt=r.scale||1),mt=r.mobileDeceleration||x,dt=r.smoothScrolling!==!1,gt=r.smoothScrollingDuration||E,vt={targetTop:it.getScrollTop()},Gt=(r.mobileCheck||function(){return/Android|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent||navigator.vendor||e.opera)})(),Gt?(st=t.getElementById("skrollr-body"),st&&at(),X(),Dt(o,[y,S],[T])):Dt(o,[y,b],[T]),it.refresh(),St(e,"resize orientationchange",function(){var e=o.clientWidth,t=o.clientHeight;(t!==$t||e!==Mt)&&($t=t,Mt=e,_t=!0);});var i=Y();return function l(){Z(),bt=i(l);}(),it}var o,a,i={get:function(){return it},init:function(e){return it||new n(e)},VERSION:"0.6.26"},l=Object.prototype.hasOwnProperty,s=e.Math,c=e.getComputedStyle,f="touchstart",u="touchmove",m="touchcancel",p="touchend",d="skrollable",g=d+"-before",v=d+"-between",h=d+"-after",y="skrollr",T="no-"+y,b=y+"-desktop",S=y+"-mobile",k="linear",w=1e3,x=.004,E=200,A="start",F="end",C="center",D="bottom",H="___skrollable_id",I=/^(?:input|textarea|button|select)$/i,P=/^\s+|\s+$/g,N=/^data(?:-(_\w+))?(?:-?(-?\d*\.?\d+p?))?(?:-?(start|end|top|center|bottom))?(?:-?(top|center|bottom))?$/,O=/\s*(@?[\w\-\[\]]+)\s*:\s*(.+?)\s*(?:;|$)/gi,V=/^(@?[a-z\-]+)\[(\w+)\]$/,z=/-([a-z0-9_])/g,q=function(e,t){return t.toUpperCase()},L=/[\-+]?[\d]*\.?[\d]+/g,M=/\{\?\}/g,$=/rgba?\(\s*-?\d+\s*,\s*-?\d+\s*,\s*-?\d+/g,_=/[a-z\-]+-gradient/g,B="",G="",K=function(){var e=/^(?:O|Moz|webkit|ms)|(?:-(?:o|moz|webkit|ms)-)/;if(c){var t=c(a,null);for(var n in t)if(B=n.match(e)||+n==n&&t[n].match(e))break;if(!B)return B=G="",r;B=B[0],"-"===B.slice(0,1)?(G=B,B={"-webkit-":"webkit","-moz-":"Moz","-ms-":"ms","-o-":"O"}[B]):G="-"+B.toLowerCase()+"-";}},Y=function(){var t=e.requestAnimationFrame||e[B.toLowerCase()+"RequestAnimationFrame"],r=Pt();return(Gt||!t)&&(t=function(t){var n=Pt()-r,o=s.max(0,1e3/60-n);return e.setTimeout(function(){r=Pt(),t();},o)}),t},R=function(){var t=e.cancelAnimationFrame||e[B.toLowerCase()+"CancelAnimationFrame"];return(Gt||!t)&&(t=function(t){return e.clearTimeout(t)}),t},U={begin:function(){return 0},end:function(){return 1},linear:function(e){return e},quadratic:function(e){return e*e},cubic:function(e){return e*e*e},swing:function(e){return-s.cos(e*s.PI)/2+.5},sqrt:function(e){return s.sqrt(e)},outCubic:function(e){return s.pow(e-1,3)+1},bounce:function(e){var t;if(.5083>=e)t=3;else if(.8489>=e)t=9;else if(.96208>=e)t=27;else{if(!(.99981>=e))return 1;t=91;}return 1-s.abs(3*s.cos(1.028*e*t)/t)}};n.prototype.refresh=function(e){var n,o,a=!1;for(e===r?(a=!0,lt=[],Bt=0,e=t.getElementsByTagName("*")):e.length===r&&(e=[e]),n=0,o=e.length;o>n;n++){var i=e[n],l=i,s=[],c=dt,f=yt,u=!1;if(a&&H in i&&delete i[H],i.attributes){for(var m=0,p=i.attributes.length;p>m;m++){var g=i.attributes[m];if("data-anchor-target"!==g.name)if("data-smooth-scrolling"!==g.name)if("data-edge-strategy"!==g.name)if("data-emit-events"!==g.name){var v=g.name.match(N);if(null!==v){var h={props:g.value,element:i,eventType:g.name.replace(z,q)};s.push(h);var y=v[1];y&&(h.constant=y.substr(1));var T=v[2];/p$/.test(T)?(h.isPercentage=!0,h.offset=(0|T.slice(0,-1))/100):h.offset=0|T;var b=v[3],S=v[4]||b;b&&b!==A&&b!==F?(h.mode="relative",h.anchors=[b,S]):(h.mode="absolute",b===F?h.isEnd=!0:h.isPercentage||(h.offset=h.offset*Vt));}}else u=!0;else f=g.value;else c="off"!==g.value;else if(l=t.querySelector(g.value),null===l)throw'Unable to find anchor target "'+g.value+'"'}if(s.length){var k,w,x;!a&&H in i?(x=i[H],k=lt[x].styleAttr,w=lt[x].classAttr):(x=i[H]=Bt++,k=i.style.cssText,w=Ct(i)),lt[x]={element:i,styleAttr:k,classAttr:w,anchorTarget:l,keyFrames:s,smoothScrolling:c,edgeStrategy:f,emitEvents:u,lastFrameIndex:-1},Dt(i,[d],[]);}}}for(Et(),n=0,o=e.length;o>n;n++){var E=lt[e[n][H]];E!==r&&(J(E),et(E));}return it},n.prototype.relativeToAbsolute=function(e,t,r){var n=o.clientHeight,a=e.getBoundingClientRect(),i=a.top,l=a.bottom-a.top;return t===D?i-=n:t===C&&(i-=n/2),r===D?i+=l:r===C&&(i+=l/2),i+=it.getScrollTop(),0|i+.5},n.prototype.animateTo=function(e,t){t=t||{};var n=Pt(),o=it.getScrollTop();return pt={startTop:o,topDiff:e-o,targetTop:e,duration:t.duration||w,startTime:n,endTime:n+(t.duration||w),easing:U[t.easing||k],done:t.done},pt.topDiff||(pt.done&&pt.done.call(it,!1),pt=r),it},n.prototype.stopAnimateTo=function(){pt&&pt.done&&pt.done.call(it,!0),pt=r;},n.prototype.isAnimatingTo=function(){return!!pt},n.prototype.isMobile=function(){return Gt},n.prototype.setScrollTop=function(t,r){return ht=r===!0,Gt?Kt=s.min(s.max(t,0),Ot):e.scrollTo(0,t),it},n.prototype.getScrollTop=function(){return Gt?Kt:e.pageYOffset||o.scrollTop||a.scrollTop||0},n.prototype.getMaxScrollTop=function(){return Ot},n.prototype.on=function(e,t){return ct[e]=t,it},n.prototype.off=function(e){return delete ct[e],it},n.prototype.destroy=function(){var e=R();e(bt),wt(),Dt(o,[T],[y,b,S]);for(var t=0,n=lt.length;n>t;t++)ot(lt[t].element);o.style.overflow=a.style.overflow="",o.style.height=a.style.height="",st&&i.setStyle(st,"transform","none"),it=r,st=r,ct=r,ft=r,Ot=0,Vt=1,ut=r,mt=r,zt="down",qt=-1,Mt=0,$t=0,_t=!1,pt=r,dt=r,gt=r,vt=r,ht=r,Bt=0,yt=r,Gt=!1,Kt=0,Tt=r;};var X=function(){var n,i,l,c,d,g,v,h,y,T,b,S;St(o,[f,u,m,p].join(" "),function(e){var o=e.changedTouches[0];for(c=e.target;3===c.nodeType;)c=c.parentNode;switch(d=o.clientY,g=o.clientX,T=e.timeStamp,I.test(c.tagName)||e.preventDefault(),e.type){case f:n&&n.blur(),it.stopAnimateTo(),n=c,i=v=d,l=g,y=T;break;case u:I.test(c.tagName)&&t.activeElement!==c&&e.preventDefault(),h=d-v,S=T-b,it.setScrollTop(Kt-h,!0),v=d,b=T;break;default:case m:case p:var a=i-d,k=l-g,w=k*k+a*a;if(49>w){if(!I.test(n.tagName)){n.focus();var x=t.createEvent("MouseEvents");x.initMouseEvent("click",!0,!0,e.view,1,o.screenX,o.screenY,o.clientX,o.clientY,e.ctrlKey,e.altKey,e.shiftKey,e.metaKey,0,null),n.dispatchEvent(x);}return}n=r;var E=h/S;E=s.max(s.min(E,3),-3);var A=s.abs(E/mt),F=E*A+.5*mt*A*A,C=it.getScrollTop()-F,D=0;C>Ot?(D=(Ot-C)/F,C=Ot):0>C&&(D=-C/F,C=0),A*=1-D,it.animateTo(0|C+.5,{easing:"outCubic",duration:A});}}),e.scrollTo(0,0),o.style.overflow=a.style.overflow="hidden";},j=function(){var e,t,r,n,a,i,l,c,f,u,m,p=o.clientHeight,d=At();for(c=0,f=lt.length;f>c;c++)for(e=lt[c],t=e.element,r=e.anchorTarget,n=e.keyFrames,a=0,i=n.length;i>a;a++)l=n[a],u=l.offset,m=d[l.constant]||0,l.frame=u,l.isPercentage&&(u*=p,l.frame=u),"relative"===l.mode&&(ot(t),l.frame=it.relativeToAbsolute(r,l.anchors[0],l.anchors[1])-u,ot(t,!0)),l.frame+=m,ft&&!l.isEnd&&l.frame>Ot&&(Ot=l.frame);for(Ot=s.max(Ot,Ft()),c=0,f=lt.length;f>c;c++){for(e=lt[c],n=e.keyFrames,a=0,i=n.length;i>a;a++)l=n[a],m=d[l.constant]||0,l.isEnd&&(l.frame=Ot-l.offset+m);e.keyFrames.sort(Nt);}},W=function(e,t){for(var r=0,n=lt.length;n>r;r++){var o,a,s=lt[r],c=s.element,f=s.smoothScrolling?e:t,u=s.keyFrames,m=u.length,p=u[0],y=u[u.length-1],T=p.frame>f,b=f>y.frame,S=T?p:y,k=s.emitEvents,w=s.lastFrameIndex;if(T||b){if(T&&-1===s.edge||b&&1===s.edge)continue;switch(T?(Dt(c,[g],[h,v]),k&&w>-1&&(xt(c,p.eventType,zt),s.lastFrameIndex=-1)):(Dt(c,[h],[g,v]),k&&m>w&&(xt(c,y.eventType,zt),s.lastFrameIndex=m)),s.edge=T?-1:1,s.edgeStrategy){case"reset":ot(c);continue;case"ease":f=S.frame;break;default:case"set":var x=S.props;for(o in x)l.call(x,o)&&(a=nt(x[o].value),0===o.indexOf("@")?c.setAttribute(o.substr(1),a):i.setStyle(c,o,a));continue}}else 0!==s.edge&&(Dt(c,[d,v],[g,h]),s.edge=0);for(var E=0;m-1>E;E++)if(f>=u[E].frame&&u[E+1].frame>=f){var A=u[E],F=u[E+1];for(o in A.props)if(l.call(A.props,o)){var C=(f-A.frame)/(F.frame-A.frame);C=A.props[o].easing(C),a=rt(A.props[o].value,F.props[o].value,C),a=nt(a),0===o.indexOf("@")?c.setAttribute(o.substr(1),a):i.setStyle(c,o,a);}k&&w!==E&&("down"===zt?xt(c,A.eventType,zt):xt(c,F.eventType,zt),s.lastFrameIndex=E);break}}},Z=function(){_t&&(_t=!1,Et());var e,t,n=it.getScrollTop(),o=Pt();if(pt)o>=pt.endTime?(n=pt.targetTop,e=pt.done,pt=r):(t=pt.easing((o-pt.startTime)/pt.duration),n=0|pt.startTop+t*pt.topDiff),it.setScrollTop(n,!0);else if(!ht){var a=vt.targetTop-n;a&&(vt={startTop:qt,topDiff:n-qt,targetTop:n,startTime:Lt,endTime:Lt+gt}),vt.endTime>=o&&(t=U.sqrt((o-vt.startTime)/gt),n=0|vt.startTop+t*vt.topDiff);}if(Gt&&st&&i.setStyle(st,"transform","translate(0, "+-Kt+"px) "+Tt),ht||qt!==n){zt=n>qt?"down":qt>n?"up":zt,ht=!1;var l={curTop:n,lastTop:qt,maxTop:Ot,direction:zt},s=ct.beforerender&&ct.beforerender.call(it,l);s!==!1&&(W(n,it.getScrollTop()),qt=n,ct.render&&ct.render.call(it,l)),e&&e.call(it,!1);}Lt=o;},J=function(e){for(var t=0,r=e.keyFrames.length;r>t;t++){for(var n,o,a,i,l=e.keyFrames[t],s={};null!==(i=O.exec(l.props));)a=i[1],o=i[2],n=a.match(V),null!==n?(a=n[1],n=n[2]):n=k,o=o.indexOf("!")?Q(o):[o.slice(1)],s[a]={value:o,easing:U[n]};l.props=s;}},Q=function(e){var t=[];return $.lastIndex=0,e=e.replace($,function(e){return e.replace(L,function(e){return 100*(e/255)+"%"})}),G&&(_.lastIndex=0,e=e.replace(_,function(e){return G+e})),e=e.replace(L,function(e){return t.push(+e),"{?}"}),t.unshift(e),t},et=function(e){var t,r,n={};for(t=0,r=e.keyFrames.length;r>t;t++)tt(e.keyFrames[t],n);for(n={},t=e.keyFrames.length-1;t>=0;t--)tt(e.keyFrames[t],n);},tt=function(e,t){var r;for(r in t)l.call(e.props,r)||(e.props[r]=t[r]);for(r in e.props)t[r]=e.props[r];},rt=function(e,t,r){var n,o=e.length;if(o!==t.length)throw"Can't interpolate between \""+e[0]+'" and "'+t[0]+'"';var a=[e[0]];for(n=1;o>n;n++)a[n]=e[n]+(t[n]-e[n])*r;return a},nt=function(e){var t=1;return M.lastIndex=0,e[0].replace(M,function(){return e[t++]})},ot=function(e,t){e=[].concat(e);for(var r,n,o=0,a=e.length;a>o;o++)n=e[o],r=lt[n[H]],r&&(t?(n.style.cssText=r.dirtyStyleAttr,Dt(n,r.dirtyClassAttr)):(r.dirtyStyleAttr=n.style.cssText,r.dirtyClassAttr=Ct(n),n.style.cssText=r.styleAttr,Dt(n,r.classAttr)));},at=function(){Tt="translateZ(0)",i.setStyle(st,"transform",Tt);var e=c(st),t=e.getPropertyValue("transform"),r=e.getPropertyValue(G+"transform"),n=t&&"none"!==t||r&&"none"!==r;n||(Tt="");};i.setStyle=function(e,t,r){var n=e.style;if(t=t.replace(z,q).replace("-",""),"zIndex"===t)n[t]=isNaN(r)?r:""+(0|r);else if("float"===t)n.styleFloat=n.cssFloat=r;else try{B&&(n[B+t.slice(0,1).toUpperCase()+t.slice(1)]=r),n[t]=r;}catch(o){}};var it,lt,st,ct,ft,ut,mt,pt,dt,gt,vt,ht,yt,Tt,bt,St=i.addEvent=function(t,r,n){var o=function(t){return t=t||e.event,t.target||(t.target=t.srcElement),t.preventDefault||(t.preventDefault=function(){t.returnValue=!1,t.defaultPrevented=!0;}),n.call(this,t)};r=r.split(" ");for(var a,i=0,l=r.length;l>i;i++)a=r[i],t.addEventListener?t.addEventListener(a,n,!1):t.attachEvent("on"+a,o),Yt.push({element:t,name:a,listener:n});},kt=i.removeEvent=function(e,t,r){t=t.split(" ");for(var n=0,o=t.length;o>n;n++)e.removeEventListener?e.removeEventListener(t[n],r,!1):e.detachEvent("on"+t[n],r);},wt=function(){for(var e,t=0,r=Yt.length;r>t;t++)e=Yt[t],kt(e.element,e.name,e.listener);Yt=[];},xt=function(e,t,r){ct.keyframe&&ct.keyframe.call(it,e,t,r);},Et=function(){var e=it.getScrollTop();Ot=0,ft&&!Gt&&(a.style.height=""),j(),ft&&!Gt&&(a.style.height=Ot+o.clientHeight+"px"),Gt?it.setScrollTop(s.min(it.getScrollTop(),Ot)):it.setScrollTop(e,!0),ht=!0;},At=function(){var e,t,r=o.clientHeight,n={};for(e in ut)t=ut[e],"function"==typeof t?t=t.call(it):/p$/.test(t)&&(t=t.slice(0,-1)/100*r),n[e]=t;return n},Ft=function(){var e=st&&st.offsetHeight||0,t=s.max(e,a.scrollHeight,a.offsetHeight,o.scrollHeight,o.offsetHeight,o.clientHeight);return t-o.clientHeight},Ct=function(t){var r="className";return e.SVGElement&&t instanceof e.SVGElement&&(t=t[r],r="baseVal"),t[r]},Dt=function(t,n,o){var a="className";if(e.SVGElement&&t instanceof e.SVGElement&&(t=t[a],a="baseVal"),o===r)return t[a]=n,r;for(var i=t[a],l=0,s=o.length;s>l;l++)i=It(i).replace(It(o[l])," ");i=Ht(i);for(var c=0,f=n.length;f>c;c++)-1===It(i).indexOf(It(n[c]))&&(i+=" "+n[c]);t[a]=Ht(i);},Ht=function(e){return e.replace(P,"")},It=function(e){return" "+e+" "},Pt=Date.now||function(){return+new Date},Nt=function(e,t){return e.frame-t.frame},Ot=0,Vt=1,zt="down",qt=-1,Lt=Pt(),Mt=0,$t=0,_t=!1,Bt=0,Gt=!1,Kt=0,Yt=[];"function"==typeof undefined&&undefined.amd?undefined("skrollr",function(){return i}):"undefined"!='object'&&module.exports?module.exports=i:e.skrollr=i;})(window,document);
});

/*! skrollr-stylesheets 1.0.0 (2014-12-12) | Alexander Prinzhorn - https://github.com/Prinzhorn/skrollr-stylesheets | Free to use under terms of MIT license */
(function(e,t){"use strict";var n,r=[],l=/@-skrollr-keyframes\s+([\w-]+)/g,s=/\s*\{\s*((?:[^{]+\{[^}]*\}\s*)+?)\s*\}/g,a=/([\w\-]+)\s*\{([^}]+)\}/g,o="skrollr-",i=/-skrollr-animation-name\s*:\s*([\w-]+)/g,u=/-skrollr-(anchor-target|smooth-scrolling|emit-events|menu-offset)\s*:\s*['"]([^'"]+)['"]/g,f=function(t){var n=new XMLHttpRequest;try{n.open("GET",t,!1),n.send(null);}catch(r){e.XDomainRequest&&(n=new XDomainRequest,n.open("GET",t,!1),n.send(null));}return n.responseText},c=function(t){for(var l=0;t.length>l;l++){var s=t[l];if("LINK"===s.tagName){if(null===s.getAttribute("data-skrollr-stylesheet"))continue;if(e.matchMedia){var a=s.getAttribute("media");if(a&&!matchMedia(a).matches)continue}n=f(s.href);}else n=s.textContent||s.innerText||s.innerHTML;n&&r.push(n);}r.reverse();for(var o={},i=[],u=[],c=0;r.length>c;c++)n=r[c],g(n,o),d(n,i),x(n,u);m(o,i),v(u);},g=function(e,t){l.lastIndex=0;for(var n,r,o,i;null!==(n=l.exec(e));)for(s.lastIndex=l.lastIndex,r=s.exec(e),a.lastIndex=0,i=t[n[1]]={};null!==(o=a.exec(r[1]));)i[o[1]]=o[2].replace(/[\n\r\t]/g,"");},h=function(e,t){for(var n,r=t;r--&&"{"!==e.charAt(r););for(n=r;n--&&"}"!==e.charAt(n-1););return e.substring(n,r).replace(/[\n\r\t]/g,"")},d=function(e,t){var n,r;for(i.lastIndex=0;null!==(n=i.exec(e));)r=h(e,i.lastIndex),t.push([r,n[1]]);},x=function(e,t){var n,r;for(u.lastIndex=0;null!==(n=u.exec(e));)r=h(e,u.lastIndex),t.push([r,n[1],n[2]]);},m=function(e,n){for(var r,l,s,a,i,u,f,c,g=0;n.length>g;g++)if(r=t.querySelectorAll(n[g][0])){l=e[n[g][1]];for(s in l)for(a=0===s.indexOf(o)?s.substring(o.length):s,i=0;r.length>i;i++)c=r[i],u="data-"+a,f=l[s],c.hasAttribute(u)&&(f+=c.getAttribute(u)),c.setAttribute(u,f);}},v=function(e){for(var n,r,l,s,a,o=0;e.length>o;o++)if(n=e[o],r=t.querySelectorAll(n[0]),l="data-"+n[1],s=n[2],r)for(a=0;r.length>a;a++)r[a].setAttribute(l,s);};c(t.querySelectorAll("link, style"));})(window,document);

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var ViewportSupervisor = function () {
  function ViewportSupervisor(className) {
    var settings = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { container: false,
      onlyFirstAppear: true,
      bounds: false };
    classCallCheck(this, ViewportSupervisor);

    this.className = className;
    this.container = settings.container || document;

    this.onlyFirstAppear = settings.onlyFirstAppear || true;
    this.bounds = settings.bounds || this.container.getBoundingClientRect

    /* Elements to be watched */
    ();this.watched = cast2array(document.querySelectorAll(this.className));
    this.active = new Uint8Array(this.watched.length).map(function (i) {
      return true;
    });
    this.visible = new Uint8Array(this.watched.length).map(function (i) {
      return false;
    });

    var evOptions = supportsPassiveEv ? { passive: true } : false;
    __throttle("scroll", "throttledScroll", this.container);
    this.container.addEventListener("throttledScroll", this.on_scroll.bind(this), evOptions

    /* Scroll to top */
    );this.container.dispatchEvent(new Event("throttledScroll"));
  }

  createClass(ViewportSupervisor, [{
    key: "dispatch",
    value: function dispatch(ev) {
      this.container.dispatchEvent(ev);
    }

    /* Helpers */

  }, {
    key: "appear",
    value: function appear(el, i) {
      this.visible[i] = true;
      if (this.onlyFirstAppear) {
        this.active[i] = false;
      }
      this.dispatch(new CustomEvent("appear", { detail: { src: el } }));
    }
  }, {
    key: "gone",
    value: function gone(el, i) {
      console.log('gone', this.active);

      this.visible[i] = false;
      this.dispatch(new CustomEvent("gone", { detail: { src: el } }));
    }
  }, {
    key: "on_scroll",
    value: function on_scroll(ev) {
      var tgt = ev.srcElement || ev.target;

      var scrollY = tgt.scrollTop,
          scrollX = tgt.scrollLeft;

      for (var i = 0; i < this.watched.length; i++) {
        var el = this.watched[i];
        var is_active = this.active[i];
        var already_visible = this.visible[i];

        if (!is_active) continue;
        var is_visible = inBounds(el, this.bounds);

        if (is_visible && !already_visible) {
          this.appear(el, i);
        } else if (!is_visible && already_visible) {
          this.gone(el, i);
        }
      }
    }
  }]);
  return ViewportSupervisor;
}();

/*
 * Returns true if El in bounds{top, bottom, left, right} range
 */


function inBounds(el, bounds) {
  var pos = el.getBoundingClientRect();
  return pos.top > bounds.top && pos.bottom < bounds.bottom && pos.left > bounds.left && pos.right < bounds.right;
}

/*
* target is the element we want to scroll to
* duration is the scroll delay in ms
*/

function smoothScroll(target, duration) {
  var startPosition = window.scrollY || window.pageYOffset;
  var targetPosition = target.getBoundingClientRect().top;
  var distance = targetPosition - startPosition;
  var timeStart = null;

  function loop(timeCurrent) {
    if (timeStart === null) timeStart = timeCurrent;

    var timeElapsed = timeCurrent - timeStart;

    var next = easing(timeElapsed, startPosition, distance, duration);

    // Vertically scroll from the current scroll position to next
    window.scrollTo(0, next);

    // Rerun the animation until the delay(duration) expires.
    if (timeElapsed < duration) window.requestAnimationFrame(loop);
  }

  // Our good old easeInOutQuad function
  function easing(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
  }

  // Start running the animation
  window.requestAnimationFrame(loop);
}

var press = document.ontouchstart == null ? 'click' : 'touchstart';

var is_mobile = window.innerWidth <= 480; //(/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
var queryString = parseQuery(location.search);
var IS_PREVIEW = queryString.preview !== undefined;

/* Avoid FOIT ( https://www.filamentgroup.com/lab/font-events.html ) */
new fontfaceobserver_standalone("Raleway").load().then(function () {
  requestAnimationFrame(function () {
    document.documentElement.className += " fonts-loaded";
  });
}

/*
 * Contact Form 
 */
);var successMessage = document.querySelector('.contato .form-success');
var contatoForm = document.querySelector('.contato form');
var contatoCloseBtn = document.querySelector('.contato .close-btn');

function closeSuccessMessage(ev) {
  ev.preventDefault();successMessage.classList.remove(':appear');
}
function showMessage(msg) {
  var p_msg = msg.split('.').slice(0, -1).map(function (m) {
    return ['<p>', m, '</p>'];
  });
  var finalmsg = p_msg.map(function (m) {
    return m.join('');
  }).join('');

  successMessage.querySelector('.message').innerHTML = finalmsg;
  successMessage.classList.add(':appear');
}

/** Clean form values */
function cleanForm(form) {
  var inputs = cast2array(form.querySelectorAll('input:not([type="submit"]),textarea'));
  inputs.map(function (inp) {
    inp.value = '';
  });
}

/** Show field error */
function showFieldErrorMessage(invalidFields) {
  invalidFields.forEach(function (field) {
    var input = document.querySelector(field.into).querySelector('input');
    if (input) {
      input.setCustomValidity(field.message);
    }
    setTimeout(function () {
      input.setCustomValidity('');
    }, 1000);
  });
  contatoForm.reportValidity();
}

function contatoFormHandler(ev) {
  ev.preventDefault

  /* Check html5 validity */
  ();if (!contatoForm.checkValidity()) {
    contatoForm.reportValidity();
    return;
  }

  /* Get form inputs */
  var contatoInputs = cast2array(contatoForm.querySelectorAll('input:not([type="submit"]),textarea'));
  var contatoData = contatoInputs.map(function (inp) {
    return [inp.getAttribute('name'), inp.value];
  }

  /* Serialize contatoData */
  );var contatoObj = contatoData.reduce(function (prev, curr) {
    prev[curr[0]] = curr[1];
    return prev;
  }, {}

  /* as form multipart */
  );var contatoFormData = new FormData();
  for (var k in contatoObj) {
    contatoFormData.append(k, contatoObj[k]);
  }

  if (IS_PREVIEW) {
    showMessage("Obrigado pela mensagem 🖖. Em breve faremos contato 👾.");
    cleanForm(contatoForm);
    return;
  }

  /* Post to wpcf7 plugin */
  var formId = parseInt(document.querySelector('input[name="_wpcf7"]', contatoForm).value, 10);
  var wpcf7_api_url = '?rest_route=/contact-form-7/v1/contact-forms/' + formId + '/feedback';
  fetch(wpcf7_api_url, {
    method: 'POST',
    headers: { 'X-Requested-With': 'XMLHttpRequest' },
    body: contatoFormData,
    credentials: 'same-origin'
  }).then(function (resp) {
    return resp.json();
  }).then(function (resp) {
    if (resp.status == "mail_sent") {
      showMessage(resp.message);
      cleanForm(contatoForm);
    } else if (resp.status == "validation_failed") showFieldErrorMessage(resp.invalidFields);else showMessage(resp.message);
  }).catch(console.error);
}
contatoForm.addEventListener('submit', contatoFormHandler);
contatoCloseBtn.addEventListener(press, closeSuccessMessage);

function start() {

  if (!is_mobile) {
    skrollr_min.init({
      smoothScrolling: true,
      smoothScrollingDuration: 33,
      forceHeight: false,
      contants: {
        box: '100p'
      }
    });
  }

  /* Mobile hack */
  if (is_mobile) {
    /* Remove those elements from tree */
    var els = document.querySelectorAll('#cremer, #pompom, #sapeka');
    els.forEach(function (el) {
      el.parentNode.removeChild(el);
    }

    /* Always mantain clientes even to avoid layout break */
    );var client_balls = document.querySelectorAll('.client-icone');
    if (client_balls.length % 2 != 0) {
      var last_ball = client_balls[client_balls.length - 1];
      last_ball.parentNode.removeChild(last_ball);
    }
  }

  /* Animate on appear classes .appear  */
  var appearAnimation = function appearAnimation(ev) {
    ev.detail.src.classList.add(':anim-play');
  };
  var goneAnimation = function goneAnimation(ev) {
    ev.detail.src.classList.remove(':anim-play');
  };

  document.addEventListener('appear', appearAnimation);
  document.addEventListener('gone', goneAnimation);

  var viewportSupervisor = new ViewportSupervisor('.\\:anim-in-viewport ', {
    bounds: { left: 0, top: 0,
      right: window.innerWidth, bottom: window.innerHeight } });

  /* Lazy load iframes */
  lf('.lazyframe', {
    /* Avoid staff picks with &loop=1 and stopping at start */
    onAppend: function onAppend(iframe) {
      var player = new window.Vimeo.Player(iframe);
      var first_play = true;
      player.on('play', function (t) {
        if (!first_play) return;
        first_play = false;

        player.addCuePoint(t.duration - 0.3);
        player.on('cuepoint', function (cue) {
          player.unload();
        });
      });
    }
  }

  /* Seta */
  );var seta = document.querySelector('.seta');
  var scrollTgt = document.querySelector('#startanchor');
  seta.addEventListener(press, function (ev) {
    smoothScroll(scrollTgt, 1000);
  });
}

document.addEventListener('DOMContentLoaded', function () {
  requestAnimationFrame(start);
});
